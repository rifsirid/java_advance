package id.go.kemenkeu.ridho.java_advance;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import id.go.kemenkeu.ridho.java_advance.entity.user.Student;
import id.go.kemenkeu.ridho.java_advance.repository.DbUser.StudentRepository;
import id.go.kemenkeu.ridho.java_advance.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.junit.jupiter.api.Assertions;

@SpringBootTest
class JavaAdvanceApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	StudentService studentService;

	@Autowired
	StudentRepository studentRepo;

	@Test
	void testUpdateEmp(){
		Student emp = new Student();
		emp.setId(1L);
		emp.setName("student");
		emp.setRole("ADMIN");
		emp.setActive(true);

		Student dataUpdate = new Student();
		dataUpdate.setId(2L);
		dataUpdate.setName("new student");
		dataUpdate.setRole("ADMIN");
		dataUpdate.setActive(true);

		Student emp3 = studentService.update(dataUpdate, 1L);
		Assertions.assertEquals(dataUpdate, emp3);
	}


}
