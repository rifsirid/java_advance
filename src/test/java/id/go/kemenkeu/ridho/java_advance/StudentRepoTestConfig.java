package id.go.kemenkeu.ridho.java_advance;

import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.go.kemenkeu.ridho.java_advance.repository.DbUser.StudentRepository;

@RunWith(MockitoJUnitRunner.class)
@Configuration
public class StudentRepoTestConfig {
    @Mock StudentRepository studentRepository;
    @Bean
    public StudentRepository studentRepository() {
        return studentRepository;
    }
}
