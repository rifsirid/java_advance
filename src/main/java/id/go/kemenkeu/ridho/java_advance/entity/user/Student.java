package id.go.kemenkeu.ridho.java_advance.entity.user;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@NamedQuery(name = "Student.findAdmin", query = "SELECT e FROM Student e WHERE e.role = 'ADMIN'")
public class Student {
    private @Id @GeneratedValue Long id;
    
    @NotBlank
    @Size(min = 3, max = 20)
    private String name;
    private String role;
    private boolean isActive;

    public Student() {
    }
    public Student(String name, String role) {
        this.name = name;
        this.role = role;
    }



    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(!(obj instanceof Student))
            return false;
        Student e = (Student) obj;
        return Objects.equals(this.id,e.id);
    }

    @Override
    public String toString() {
        return super.toString();
    }
   
    public Long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getRole() {
        return role;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public boolean isActive() {
        return isActive;
    }
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    
}
