package id.go.kemenkeu.ridho.java_advance.repository.DbUser;

import org.springframework.data.jpa.repository.JpaRepository;

import id.go.kemenkeu.ridho.java_advance.entity.user.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
   

    
}
