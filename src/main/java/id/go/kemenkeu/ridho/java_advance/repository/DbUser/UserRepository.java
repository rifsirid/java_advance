package id.go.kemenkeu.ridho.java_advance.repository.DbUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;

import id.go.kemenkeu.ridho.java_advance.entity.user.User;

public interface UserRepository extends JpaRepository<User, Long>{

    UserDetails findByUsername(String username);

}
    

