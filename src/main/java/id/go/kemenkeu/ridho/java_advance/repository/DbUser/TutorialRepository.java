package id.go.kemenkeu.ridho.java_advance.repository.DbUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.go.kemenkeu.ridho.java_advance.entity.tutorial.Tutorial;


@Repository
public interface TutorialRepository extends JpaRepository<Tutorial, Integer> {
}
