package id.go.kemenkeu.ridho.java_advance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.go.kemenkeu.ridho.java_advance.entity.user.Student;
import id.go.kemenkeu.ridho.java_advance.repository.DbUser.StudentRepository;

@Service
public class StudentService {
    @Autowired
    private StudentRepository repo;

    public Student update(Student newEmp, Long id) {
        return repo.findById(id)
                .map(emp -> {
                    emp.setName(newEmp.getName());
                    emp.setActive(newEmp.isActive());
                    emp.setRole(newEmp.getRole());
                    return repo.save(emp);
                })
                .orElseGet(() -> {
                    newEmp.setId(id);
                    return repo.save(newEmp);
                });
    }
    
    public List<Student> findUser(){
        return repo.findUser();
    }
}
