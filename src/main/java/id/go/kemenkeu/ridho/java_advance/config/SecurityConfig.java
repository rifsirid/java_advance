package id.go.kemenkeu.ridho.java_advance.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import id.go.kemenkeu.ridho.java_advance.security.JwtAutheticationFilter;
import id.go.kemenkeu.ridho.java_advance.security.JwtProvider;
import id.go.kemenkeu.ridho.java_advance.service.UserService;

@Configuration
public class SecurityConfig {
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * This bean is used to configure the JWT token. Configure the URLs that should not be protected by the JWT token.
     *
     * @param http the HttpSecurity object
     * @return the HttpSecurity object
     * @throws Exception if an error occurs
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, JwtProvider provider) throws Exception {
        //@formatter:off
        return http
                .authorizeHttpRequests(authorizeRequests -> authorizeRequests

                        .antMatchers("/", "/swagger-ui-custom.html" ,"/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**", "/webjars/**",
                                "/swagger-ui/index.html","/api-docs/**")
                        .permitAll()
                        .antMatchers(HttpMethod.POST,"/users").permitAll()
                        .antMatchers(HttpMethod.POST,"/login").permitAll()
                        .anyRequest()
                        .authenticated())
                .cors().disable()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(new JwtAutheticationFilter(provider), UsernamePasswordAuthenticationFilter.class)
                .build();
        //@formatter:on
    }

    @Bean
    AuthenticationManager customAuthenticationManager(UserService service, PasswordEncoder encoder){
        return authentication->{
            String username = authentication.getName();
            String password = (String) authentication.getCredentials();
            // service.loadUserByUsername(username);
            if(encoder.matches(password,service.loadUserByUsername(username).getPassword())){
                return authentication;
            }
            throw new RuntimeException("Authentication failed");
        
        };
    }

}
