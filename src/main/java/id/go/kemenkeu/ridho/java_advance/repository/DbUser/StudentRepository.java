package id.go.kemenkeu.ridho.java_advance.repository.DbUser;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.go.kemenkeu.ridho.java_advance.entity.user.Student;

public interface StudentRepository extends JpaRepository<Student, Long>{
    
    public List<Student> findByName(String name);
    
    public List<Student> findAdmin();
    
    @Query("SELECT e FROM Student e WHERE e.role = 'USER'")
    public List<Student> findUser();
    
    @Query(value="SELECT * FROM student WHERE role = 'USER' and is_active=true", nativeQuery = true)
    public List<Student> findUserBiasa();

}
