package id.go.kemenkeu.ridho.java_advance.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.sql.DataSource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(
    basePackages = "id.go.kemenkeu.ridho.java_advance.repository.DbUser",
    entityManagerFactoryRef = "userEntityManagerFactory",
    transactionManagerRef = "userTransactionManager"
)
public class DbUserConfig {
  @Autowired
  private JpaProperties jpaProperties;
  @Autowired
  private HibernateProperties hibernateProperties;

  @Bean
  @ConfigurationProperties(prefix = "spring.datasource-user")
  DataSource userDataSource(){
      return DataSourceBuilder.create().build();
  }
 
  public Map<String, Object> getVendorProperties() {
      return hibernateProperties.determineHibernateProperties(jpaProperties.getProperties(), new HibernateSettings());
  }

  @Bean(name = "userEntityManagerFactory")
  public LocalContainerEntityManagerFactoryBean userEntityManagerFactory (EntityManagerFactoryBuilder builder) {
      return builder
              .dataSource(userDataSource())
              .properties(getVendorProperties())
              .packages("id.go.kemenkeu.ridho.java_advance.entity")
              .persistenceUnit("primaryPersistenceUnit")
              .build();
  }

  @Bean(name = "entityManagerPrimary")
  public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
      return userEntityManagerFactory(builder).getObject().createEntityManager();
  }

  @Bean(name = "userTransactionManager")
  @Primary
  PlatformTransactionManager userTransactionManager(EntityManagerFactoryBuilder builder) {
      return new JpaTransactionManager(userEntityManagerFactory(builder).getObject());
  }
    
    
}
