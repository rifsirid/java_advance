package id.go.kemenkeu.ridho.java_advance.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import id.go.kemenkeu.ridho.java_advance.entity.user.Student;
import id.go.kemenkeu.ridho.java_advance.repository.DbUser.StudentRepository;
import id.go.kemenkeu.ridho.java_advance.service.StudentService;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
public class StudentController {
    private final StudentRepository repo;
    StudentController(StudentRepository repository){
        this.repo = repository;
    }
    @Autowired
    private StudentService service;

    @Operation(summary = "Get Semua student dengan role user")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the student"),
        @ApiResponse(responseCode = "404", description = "Student not found"),
        @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    @GetMapping("/students_user")
    @RateLimiter(name = "student")
    public List<Student> findUser(){
        return service.findUser();
    }

    @Operation(summary = "Get Semua student dengan role user yang aktif")
    @GetMapping("/students_active_user")
    public List<Student> findActiveUser(){
        return repo.findUserBiasa();
    }
    @GetMapping("/students_admin")
    public List<Student> getAdmin(){
        return repo.findAdmin();
    }

    @GetMapping("/students_by_name/{name}")
    public List<Student> getStudentByName(@PathVariable String name){
        return repo.findByName(name);
    }

    @GetMapping("/students")
    Page<Student> getAll(){
        return repo.findAll(
            PageRequest.of(0, 5, Sort.by("name").ascending())      
        );
    }

    @RateLimiter(name = "student")
    @PostMapping("/students")
    Student add(@RequestBody Student newEmp){
        return repo.save(newEmp);
    }
    @GetMapping("/students/{id}")
    Student getOne(@PathVariable Long id){
        return repo.findById(id).orElseThrow(()->new StudentNotFoundException(id));
    }
    @RateLimiter(name = "student")
    @DeleteMapping("/students/{id}")
    void deleteEmp(@PathVariable Long id){
        repo.deleteById(id);
    }

    
    @RateLimiter(name = "student")
    @PutMapping("/students/{id}")
    Student updateEmp(@RequestBody Student newEmp, @PathVariable Long id){
        return service.update(newEmp, id);
    }

    private static String UPLOAD_FOLDER = "/tmp/uploads/";
    @PostMapping(value = "/students/upload", consumes = "multipart/form-data")
    public String uploadFile(@RequestParam("file") MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            Path dir = Paths.get(UPLOAD_FOLDER);
            if (!Files.exists(dir)) {
                Files.createDirectories(dir);
            }
            
            Path path = Paths.get(UPLOAD_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
            return "File uploaded successfully!";
        } catch (IOException e) {
            e.printStackTrace();
            return "Gagal upload file!";
        }
    }

    @PostMapping(value = "/students/uploads", consumes = "multipart/form-data")
    public String uploadFiles(@RequestParam("file") MultipartFile[] file) {
        try {
            for (MultipartFile f : file) {
                byte[] bytes = f.getBytes();
                Path dir = Paths.get(UPLOAD_FOLDER);
                //jika directory belum ada maka akan dibuat
                if (!Files.exists(dir)) {
                    Files.createDirectories(dir);
                }
                //set path untuk file yang akan diupload
                Path path = Paths.get(UPLOAD_FOLDER + f.getOriginalFilename());
                Files.write(path, bytes);
            }
            return "File uploaded successfully!";
        } catch (IOException e) {
            e.printStackTrace();
            return "Gagal upload file!";
        }
    }

    @GetMapping("/students/download")
    public Resource downloadFile(String fileName) throws IOException {
        //  Path path = Paths.get(UPLOAD_FOLDER + fileName);
         File file = new File(UPLOAD_FOLDER + fileName); 
         HttpHeaders header = new HttpHeaders();
         header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName());
         header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(file.toPath()));
        return resource;
    }
}
