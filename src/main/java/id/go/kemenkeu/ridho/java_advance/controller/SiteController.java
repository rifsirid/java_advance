package id.go.kemenkeu.ridho.java_advance.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;

@RestController
public class SiteController {
    @RateLimiter(name = "student")
    @GetMapping("/")
    String site(){
        return "API ver. 0.1";
    }
}
