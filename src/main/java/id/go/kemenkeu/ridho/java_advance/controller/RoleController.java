package id.go.kemenkeu.ridho.java_advance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import id.go.kemenkeu.ridho.java_advance.entity.user.Role;
import id.go.kemenkeu.ridho.java_advance.repository.DbUser.RoleRepository;
@RestController
public class RoleController {
    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/roles")
    List<Role> all() {
        return roleRepository.findAll();
    }
    
}
