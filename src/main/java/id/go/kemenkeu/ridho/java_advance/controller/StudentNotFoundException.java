package id.go.kemenkeu.ridho.java_advance.controller;

public class StudentNotFoundException extends RuntimeException {
    StudentNotFoundException(Long id){
        super(String.format("Student with id:%s not found",id));
    }
}
